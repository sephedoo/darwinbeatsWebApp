import { DbWebAppPage } from './app.po';

describe('db-web-app App', function() {
  let page: DbWebAppPage;

  beforeEach(() => {
    page = new DbWebAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
