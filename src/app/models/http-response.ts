import { Mapper } from './Serializable'
export class HttpResponse<T>  {
    constructor(
        public metadata: any,
        public data: [T]
    ) { }
};

export class RestResponse<T>{
    constructor(
        public response: HttpResponse<T>
    ) { }
};