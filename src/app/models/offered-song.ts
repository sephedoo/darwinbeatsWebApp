import { Mapper } from './Serializable'
import { ArtistModel } from './artist-model'

/**
 * 
 * 
 * @export
 * @class SongModel
 * @extends {Serializable}
 */
export class SongModel extends Mapper {
  id: string;
  title: string;
  duration: number;
  link:LinkModel
  image: string
  passed: boolean;
  user_id : ArtistModel;


  /**
   * Dedault constructor
   * @memberof SongModel
   */
  public constructor() {
    super();
    this.id = null;
    this.title = null;
    this.duration = null;
    this.link = null;
    this.image = null;
    this.passed = false;
    this.user_id = new ArtistModel();
    this.link = new LinkModel();
  }

 
}

  export class LinkModel extends Mapper{
    64:string;
    192:string;
    public constructor(){
      super()
      this[64] = null;
      this[192]= null;
    }
  }

