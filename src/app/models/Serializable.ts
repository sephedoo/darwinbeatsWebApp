export class Mapper {

    public mapFromObject(jsonObj: any) {
        for (var propName in jsonObj) {
            if (propName in this && typeof jsonObj[propName] !== "object")
                this[propName] = jsonObj[propName];
        }
    }
}