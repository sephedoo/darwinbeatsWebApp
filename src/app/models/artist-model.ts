import { Mapper } from './Serializable'

/**
 * 
 * 
 * @export
 * @class SongModel
 * @extends {Serializable}
 */
export class ArtistModel extends Mapper {
 full_name:string ;
 id:number;
 photo_link:string;
 photo_link_full:string;


  /**
   * Dedault constructor
   * @memberof SongModel
   */
  public constructor() {
    super();
    this.id = null;
    this.photo_link = null;
    this.full_name = null;
    this.photo_link_full = null;
  }


}