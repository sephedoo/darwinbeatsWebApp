import { Component, OnInit } from '@angular/core';
import { EventService } from './core/navbar.refresh.service';
import { AuthenticationService } from './core/authentication.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  isLogged: boolean;
  ngOnInit(): void {
    this.isLogged = this.authenticationService.isLogged();
  }
  constructor(private eventService: EventService, private authenticationService: AuthenticationService) {
    eventService.userIsLogged$.subscribe(data => this.isLogged = data);
  }
}

