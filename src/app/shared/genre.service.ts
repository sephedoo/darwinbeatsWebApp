import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { AuthenticationService } from '../core/authentication.service';
import { Observable } from 'rxjs/Observable';
import { Genre } from '../models/genre';
import { HttpResponse, RestResponse } from '../models/http-response';


@Injectable()
export class GenreServices {

  constructor(private http: Http, private authenticationService: AuthenticationService) {
  }

  getAlltheGenres(): Observable<RestResponse<Genre>> {
    let headers = new Headers();
    this.authenticationService.addTokenHeader(headers);
    return this.http.get('http://api.darwinbeat.com:8000/test/1.3/dim/genres', { headers: headers }).map((response: Response) => {
      return response.json();
    });
  }
}

