import { Component, OnInit ,HostListener} from '@angular/core';
import { EventService } from '../../core/navbar.refresh.service'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(private evenService:EventService ) { }

  @HostListener('window:scroll', ['$event']) 
    doSomething(event) {
      if(window.pageYOffset!==0)
      this.evenService.refresh(true);
      else
      this.evenService.refresh(false);
    }

  ngOnInit() {
  }

}
