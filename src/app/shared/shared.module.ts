import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { AboutComponent } from './about/about.component';
import { AppRoutingModule } from '../app-routing.module';
import { LoginComponent } from './login/login.component';
import { MusicplayerComponent } from './musicplayer/musicplayer.component'
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PlayerMusicBarComponent } from './musicplayer/playermusicbar/playermusicbar.component'
import { GenreServices } from './genre.service'
import { OfferSongService } from './offersong.service'
import { ProfilesettingComponent } from './profilesetting/profilesetting.component'
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FacebookModule } from 'ng2-facebook-sdk';

import {
  MdButtonModule, MdSliderModule, MdGridListModule, MdProgressBarModule, MdSlideToggleModule, MdChipsModule,
  MdTooltipModule, MdIconModule, MdCheckboxModule, MdInputContainer, MdInputDirective, MdCardModule,
  MdToolbarModule, MdSelectModule, MdMenuModule, MdListModule,MdProgressSpinnerModule
} from '@angular/material';
import { ImageholderComponent } from './musicplayer/imageholder/imageholder.component';
import { SongdeatilComponent } from './musicplayer/songdeatil/songdeatil.component';
import { ControllerComponent } from './musicplayer/controller/controller.component';
import { FacebookloginComponent } from './login/facebooklogin/facebooklogin.component';
import { ToMinutePipe } from './to-minute.pipe';

@NgModule({
  imports: [
    FacebookModule.forRoot(),
    MdMenuModule,
    MdProgressSpinnerModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MdButtonModule,
    MdCheckboxModule,
    MdIconModule,
    MdCardModule,
    MdToolbarModule,
    MdSelectModule,
    MdChipsModule,
    MdSlideToggleModule,
    MdTooltipModule,
    MdProgressBarModule,
    MdGridListModule,
    MdSliderModule,
    MdListModule
  ],
  exports:
  [
    NavbarComponent,
    AboutComponent,
    ProfilesettingComponent,
    MdInputContainer,
    MusicplayerComponent,
    PlayerMusicBarComponent
  ],
  declarations:
  [
    ProfilesettingComponent,
    NavbarComponent,
    AboutComponent,
    LoginComponent,
    MdInputContainer,
    MdInputDirective,
    MusicplayerComponent,
    PlayerMusicBarComponent,
    ImageholderComponent,
    SongdeatilComponent,
    ControllerComponent,
    FacebookloginComponent,
    ToMinutePipe
  ],
  providers: [
    GenreServices,
    OfferSongService
  ]
})
export class SharedModule { }
