import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GenreServices } from '../genre.service';
import { Genre } from '../../models/genre';
import { HttpResponse, RestResponse } from '../../models/http-response';



@Component({
  selector: 'app-profilesetting',
  templateUrl: './profilesetting.component.html',
  styleUrls: ['./profilesetting.component.css']
})
export class ProfilesettingComponent implements OnInit {

  result: HttpResponse<Genre>;
  selectedValue: string;
  genres: Genre[] = [];
  selectedGenres = new Set<string>([]);

  onChange(newValue) {
    if (newValue !== undefined)
      this.selectedGenres.add(newValue);
  }

  constructor(private genresServices: GenreServices) { }

  ngOnInit() {
    this.genresServices.getAlltheGenres().subscribe((data) => {
      this.result = <HttpResponse<Genre>>data.response;
      this.genres = this.result.data;
    });
  }

  removeFromGenres(selected: string) {
    this.selectedGenres.delete(selected);
  }
}