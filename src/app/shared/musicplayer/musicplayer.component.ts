import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { OfferSongService } from '../offersong.service';
import { SongModel } from '../../models/offered-song';
import { HttpResponse } from '../../models/http-response';
import { MusicService } from '../../core/music.service';

@Component({
  selector: 'app-musicplayer',
  templateUrl: './musicplayer.component.html',
  styleUrls: ['./musicplayer.component.css']
})
export class MusicplayerComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    this.audio.pause();
  }

  result: HttpResponse<SongModel>;
  songs: SongModel[] = [];
  song: SongModel;
  current: number = 0
  audio: HTMLAudioElement = new Audio();
  duration: number = 0
  paused: boolean = false;


  constructor(private offerSongService: OfferSongService, private musicService: MusicService) { }
  /**
   * 
   * 
   * @memberof MusicplayerComponent
   */
  ngOnInit() {
    this.song = this.musicService.getNextMusic();
    this.audio.src = this.song.link[192];
    this.audio.play();
    this.audio.ondurationchange = () => { this.duration = this.audio.duration }
    this.audio.ontimeupdate = () => {
      this.current = this._calculateProgressBar(this.audio.currentTime)
      this.audio.onended = () => {
        this.current = 0;
        this.proceedNextMusic();
      }
    }
  }

  private _calculateProgressBar(currentTime: number): number {
    return (currentTime / this.duration) * 100;
  }

  proceedNextMusic(): void {
    this.musicService.markAsPassed(this.song);
    this.musicService.refreshCachedMusic();
    this.song = this.musicService.getNextMusic();
    this.audio.src = this.song.link[192];
    if (!this.paused) {
      this.audio.play();
    }

    this.audio.ondurationchange = () => { this.duration = this.audio.duration }
    this.audio.ontimeupdate = () => { this.current = this._calculateProgressBar(this.audio.currentTime) }
    this.audio.onended = () => {
      this.current = 0;
      this.musicService.markAsPassed(this.song);
      this.proceedNextMusic();
    }
  }

  private _pause(): void {
    if (this.paused) {
      this.paused = false
      this.audio.play();
    }
    else {
      this.paused = true
      this.audio.pause();
    }
  }
}