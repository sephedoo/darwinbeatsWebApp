import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SongdeatilComponent } from './songdeatil.component';

describe('SongdeatilComponent', () => {
  let component: SongdeatilComponent;
  let fixture: ComponentFixture<SongdeatilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SongdeatilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SongdeatilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
