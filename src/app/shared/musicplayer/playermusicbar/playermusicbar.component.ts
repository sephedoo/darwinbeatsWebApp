import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'app-playermusicbar',
  templateUrl: './playermusicbar.component.html',
  styleUrls: ['./playermusicbar.component.css']
})
export class PlayerMusicBarComponent {
  private _barPosition = '';
  @Input()
  set barPosition(barPosition: string) {
    this._barPosition = barPosition.toString() + "%" || "0%"

  }
  get barPosition(): string { return this._barPosition; }

}
