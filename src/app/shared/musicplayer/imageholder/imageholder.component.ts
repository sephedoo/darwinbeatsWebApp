import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-imageholder',
  templateUrl: './imageholder.component.html',
  styleUrls: ['./imageholder.component.css']
})
export class ImageholderComponent {

  private _ImageUrl: string = '';
  private _deafultImageUrl: string = ''
  @Input()
  set imageUrl(imageUrl: string) {
    this._ImageUrl = imageUrl.toString() || this._deafultImageUrl

  }
  get imageUrl(): string { return this._ImageUrl; }

}
