import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toMinute'
})
export class ToMinutePipe implements PipeTransform {

  transform(d: number, args?: any): any {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);
    s = (s > 60) ? 60 % s : s;
    m = (m > 60) ? 60 % m : m;

    var s1: string = s.toString();
    s1 = (s1.length===1)? '0'+s1 :s1;

    var m1: string = m.toString();
    m1 = (m1.length===1)? '0'+m1 :m1;
    return `${m1}:${s1}`
  }

}
