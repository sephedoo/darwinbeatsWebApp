import { Component, OnInit, Input } from '@angular/core';
import { EventService } from '../../core/navbar.refresh.service';
import { AuthenticationService } from '../../core/authentication.service'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  IsShadowed: boolean = false;
  isLogged: boolean;

  constructor(private eventService: EventService, private authenticationService: AuthenticationService) {
    eventService.navIsShadowed$.subscribe(
      data => {
        this.IsShadowed = data
      });
    eventService.userIsLogged$.subscribe(
      data => {
        this.isLogged = data;
      }

    )
  }
  ngOnInit(): void {
    this.isLogged = this.authenticationService.isLogged();
  }

  logOut(): void {
    this.authenticationService.logout();
  }

}
