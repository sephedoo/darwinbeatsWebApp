import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { AuthenticationService } from '../core/authentication.service';
import { Observable } from 'rxjs/Observable';
import { SongModel } from '../models/offered-song';
import { HttpResponse, RestResponse } from '../models/http-response';


@Injectable()
export class OfferSongService {

  constructor(private http: Http, private authenticationService: AuthenticationService) {
  }

  getOfferedSongs(): Observable<RestResponse<SongModel>> {
    let headers = new Headers();
    this.authenticationService.addTokenHeader(headers);
    return this.http.get('http://api.darwinbeats.com:8000/test/1.4.1/profiles/me/offer?firstTime=true&count=2'
      , { headers: headers }).map((response: Response) => {
        return response.json();
      });
  }
}
