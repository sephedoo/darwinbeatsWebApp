import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../../models/user';
import { AuthenticationService } from '../../core/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  user: User;
  logged: boolean = false;
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required)
  });


  onSubmit(value: User, valid: Boolean) {
   this.user = <User>this.loginForm.value;
    this.authenticationService.login(this.user.username, this.user.password).subscribe(log => {
      this.logged = log;
      if (this.logged === true) {
        console.log(this.logged);
        this.router.navigate(['/about']);
      }
    }, err => {
      this.logged = false;
    }
    );
  }

  ngOnInit() {
  }

}
