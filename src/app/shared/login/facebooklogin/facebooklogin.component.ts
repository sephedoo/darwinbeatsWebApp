import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../core/authentication.service'
import { FacebookService, InitParams } from 'ng2-facebook-sdk';
import{ Router} from '@angular/router'

@Component({
  selector: 'app-facebooklogin',
  templateUrl: './facebooklogin.component.html',
  styleUrls: ['./facebooklogin.component.css']
})
export class FacebookloginComponent {

  constructor(private fb: FacebookService, private authenticationService: AuthenticationService,private router:Router) {
    const params: InitParams = {
      appId: '684200201713660',
      cookie: true,
      xfbml: true,
      version: 'v2.8'
    }
    this.fb.init(params);

  }

  login(): void {
    this.fb.login().then(s => this.fb.getLoginStatus()
    .then(x => this.authenticationService.fbLogin(x.authResponse.accessToken)
    .subscribe(data => {
      if(data===true)
      {
      this.router.navigate(['/about']);
      }
      else{

      }
    })))
  }
}
