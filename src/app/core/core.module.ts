import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationService } from './authentication.service';
import { AuthGuard } from './auth.guard';
import { EventService } from './navbar.refresh.service';
import { LocalStorageService } from './localstorage.service'
import { MusicService } from './music.service'


@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [AuthenticationService, AuthGuard]
})
export class CoreModule {

  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        AuthenticationService,
        AuthGuard,
        EventService,
        LocalStorageService,
        MusicService
      ]
    };
  }
}
