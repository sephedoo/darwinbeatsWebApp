import { Injectable, OnInit } from '@angular/core';
import { LocalStorageService } from './localstorage.service'
import { OfferSongService } from '../shared/offersong.service'
import { SongModel } from '../models/offered-song'
import { HttpResponse } from "app/models/http-response";
import { ArtistModel } from '../models/artist-model'
import * as _ from "lodash";

@Injectable()
export class MusicService {

    constructor(private offerSongService: OfferSongService, private localStorageService: LocalStorageService) { }

    //properties
    song: SongModel = new SongModel();
    songs: Array<SongModel> = []
    result: HttpResponse<SongModel>;
    readonly songListKey: string = 'songListKey';


    /**
     * 
     * Get The next Music on the list
     * @returns {SongModel} 
     * @memberof MusicService
     */
    getNextMusic(): SongModel {
        if (this.localStorageService.getItem(this.songListKey) as [SongModel] === null) {
            this.offerSongService.getOfferedSongs().subscribe(data => {
                this.result = <HttpResponse<SongModel>>data.response
                _.forEach(this.result.data, x => {
                    var tempsong = new SongModel();
                    tempsong.user_id.mapFromObject(x.user_id)
                    tempsong.link.mapFromObject(x.link)
                    tempsong.mapFromObject(x);
                    this.songs.push(tempsong);
                })
                this.localStorageService.setItem<Array<SongModel>>(this.songs, this.songListKey)
                this.song = _.find(this.songs, s => s.passed === false);
                return this.song;
            });

        }
        else {
            this.songs = this.localStorageService.getItem<Array<SongModel>>(this.songListKey) as Array<SongModel>
            this.song = _.find(this.songs as Array<SongModel>, s => s.passed === false);

            return this.song
        }
    }

    refreshCachedMusic(): void {
        var cachedSongs = this.localStorageService.getItem<Array<SongModel>>(this.songListKey) as Array<SongModel>
        var nonPassedItems = _.filter(this.songs as Array<SongModel>, s => s.passed === false) as Array<SongModel>;
        if (nonPassedItems.length < 10) {
            this.offerSongService.getOfferedSongs().subscribe(data => {
                this.result = <HttpResponse<SongModel>>data.response;
                _.forEach(this.result.data, x => {
                    var tempsong = new SongModel();
                    tempsong.user_id.mapFromObject(x.user_id)
                    tempsong.link.mapFromObject(x.link)
                    tempsong.mapFromObject(x);
                    nonPassedItems.push(tempsong);
                    this.songs = nonPassedItems;
                })
                this.localStorageService.setItem<Array<SongModel>>(this.songs, this.songListKey)
                this.song = _.find(this.songs, s => s.passed === false);
            })
        }
    }
    
    


    /**
     * Mark the music as passed and user won't listen to it next time.
     * @param {SongModel} song 
     * @memberof MusicService
     */
    markAsPassed(song: SongModel): void {
        song.passed = true;
        this.songs = this.localStorageService.getItem<Array<SongModel>>(this.songListKey) as Array<SongModel>
        var index = _.findIndex(this.songs, x => x.id === song.id && x.passed===false)
        this.songs[index] = song;
        this.localStorageService.setItem<Array<SongModel>>(this.songs, this.songListKey)
    }


} 