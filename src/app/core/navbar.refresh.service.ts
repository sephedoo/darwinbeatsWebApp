import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class EventService {

  // Observable string sources
  private navShadowMode = new Subject<boolean>();
  private isLoggedModel = new Subject<boolean>()

  navIsShadowed$ = this.navShadowMode.asObservable();
  userIsLogged$ = this.isLoggedModel.asObservable();
  //send central event when scrolled down 
  refresh(isShadowed: boolean) {
    this.navShadowMode.next(isShadowed);
  }
  
  //send central event when user logged in
  loggedIn(isLogged:boolean){
    this.isLoggedModel.next(isLogged)
  }
}