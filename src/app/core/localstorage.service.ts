import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

    /**
     * set an Item in local storage
     * 
     * @template T type of object
     * @param {T} obj object to store
     * @param {string} key key for local storage
     * 
     * @memberof LocalStorageService
     */
    setItem<T>(obj: T, key: string) {
        localStorage.setItem(key, JSON.stringify(obj));
    }


    /**
     * Get a key from local storage
     * 
     * @template T 
     * @param {string} key 
     * @returns {T} 
     * 
     * @memberof LocalStorageService
     */
    getItem<T>(key: string): T {
        return JSON.parse(localStorage.getItem(key))
    }
}