import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { EventService } from './navbar.refresh.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class AuthenticationService {
	public token: string;

	constructor(private http: Http,private eventService:EventService) {
		var currentUser = JSON.parse(localStorage.getItem('currentUser'));
		this.token = currentUser && currentUser.token;
	}

	createAuthorizationHeader(headers: Headers, username: string, password: string) {
		headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
	}

	getCurrentToken() {
		return (JSON.parse(localStorage.getItem('currentUser')).token);
	}

	addTokenHeader(headers: Headers) {
		headers.append('x-access-token', this.getCurrentToken())
	}

	isLogged(): boolean {
		var token = JSON.parse(localStorage.getItem('currentUser'));
		var result: boolean = token ? true : false;
		return result;
	}

	login(username: string, password: string) {
		let headers = new Headers();
		this.createAuthorizationHeader(headers, username, password);
		return this.http.get('http://api.darwinbeat.com:8000/test/signin', { headers: headers }).map((response: Response) => {
			console.log(response);
			let token = response.json() && response.json().token;
			if (token) {
				this.token = token;
				localStorage.setItem('currentUser', JSON.stringify({ token: token }));
				this.eventService.loggedIn(true);
				return true;
			}
			else {
				return false;
			}
		});
	}

	fbLogin(fbToken: string) {
		let params: URLSearchParams = new URLSearchParams();
		params.set('token', fbToken);
		return this.http.get('http://api.darwinbeat.com:8000/test/login/facebook', { search: params }).
			map((response: Response) => {
				console.log(response.json())
				let token = response.json() && response.json().token;
				if (token) {
					this.token = token;
					localStorage.setItem('currentUser', JSON.stringify({ token: token }));
					this.eventService.loggedIn(true);
					return true;
				}
				else {
					console.log("false")
					return false;
				}
			});
	}


	logout(): void {
		this.token = null;
		localStorage.removeItem('currentUser');
		this.eventService.loggedIn(false);
	}
}