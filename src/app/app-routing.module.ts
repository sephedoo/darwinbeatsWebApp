import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { AboutComponent } from './shared/about/about.component';
import { LoginComponent } from './shared/login/login.component';
import { ProfilesettingComponent } from './shared/profilesetting/profilesetting.component'
import { AuthGuard } from './core/auth.guard'



export const routes: Routes = [
  { path: 'about', component: AboutComponent,canActivate:[AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'setting', component: ProfilesettingComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }